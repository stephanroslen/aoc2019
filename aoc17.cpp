#include <algorithm>
#include <array>
#include <deque>
#include <fstream>
#include <iostream>
#include <iterator>
#include <regex>
#include <tuple>
#include <vector>
#include <utility>

struct IntCodeMachine {
  enum State { Continue, Finished, Wait };

  using basetype = int64_t;

  static constexpr int kInstAdd{1};
  static constexpr int kInstMul{2};
  static constexpr int kInstIn{3};
  static constexpr int kInstOut{4};
  static constexpr int kInstJumpTrue{5};
  static constexpr int kInstJumpFalse{6};
  static constexpr int kInstLess{7};
  static constexpr int kInstEqual{8};
  static constexpr int kInstBase{9};
  static constexpr int kInstExit{99};

  IntCodeMachine() = default;
  IntCodeMachine(const IntCodeMachine &) = default;
  IntCodeMachine(IntCodeMachine &&) = default;
  IntCodeMachine &operator=(const IntCodeMachine &) = default;
  IntCodeMachine &operator=(IntCodeMachine &&) = default;
  ~IntCodeMachine() = default;

  basetype ip{0};
  basetype base{0};

  using OpCode = std::tuple<int, int, int, int>;

  static OpCode DecodeInst(int inst) {
    return {inst % 100, inst / 100 % 10, inst / 1000 % 10, inst / 10000 % 10};
  }

  State Step(void) {
    basetype inst = code[ip];
    OpCode opcode = DecodeInst(inst);
    auto &[op, mode0, mode1, mode2] = opcode;

    auto ensure = [&](basetype loc) -> basetype {
      if (code.size() < loc + 1) {
        code.resize(loc + 1, 0);
      }
      return loc;
    };

    auto read2 = [&](void) -> basetype {
      basetype &l = code[ip + 3];
      int mode = std::get<3>(opcode);
      switch (mode) {
        case 0:
          return code[ensure(l)];
        default:
        case 1:
          return l;
        case 2:
          return code[ensure(base + l)];
      }
    };
    auto read1 = [&](void) -> basetype {
      basetype &l = code[ip + 2];
      int mode = std::get<2>(opcode);
      switch (mode) {
        case 0:
          return code[ensure(l)];
        default:
        case 1:
          return l;
        case 2:
          return code[ensure(base + l)];
      }
    };
    auto read0 = [&](void) -> basetype {
      basetype &l = code[ip + 1];
      int mode = std::get<1>(opcode);
      switch (mode) {
        case 0:
          return code[ensure(l)];
        default:
        case 1:
          return l;
        case 2:
          return code[ensure(base + l)];
      }
    };

    auto write2 = [&](basetype val) -> void {
      basetype &l = code[ip + 3];
      int mode = std::get<3>(opcode);
      switch (mode) {
        default:
        case 0:
          code[ensure(l)] = val;
        case 2:
          code[ensure(base + l)] = val;
      }
    };
    auto write1 = [&](basetype val) -> void {
      basetype &l = code[ip + 2];
      int mode = std::get<2>(opcode);
      switch (mode) {
        default:
        case 0:
          code[ensure(l)] = val;
        case 2:
          code[ensure(base + l)] = val;
      }
    };
    auto write0 = [&](basetype val) -> void {
      basetype &l = code[ip + 1];
      int mode = std::get<1>(opcode);
      switch (mode) {
        default:
        case 0:
          code[ensure(l)] = val;
        case 2:
          code[ensure(base + l)] = val;
      }
    };

    switch (op) {
      case kInstAdd:
        write2(read0() + read1());
        break;
      case kInstMul:
        write2(read0() * read1());
        break;
      case kInstIn:
        if (inputs.size() == 0) {
          return Wait;
        }
        write0(inputs.front());
        inputs.pop_front();
        break;
      case kInstOut:
        outputs.push_back(read0());
        break;
      case kInstJumpTrue:
        if (read0()) {
          ip = read1();
          return Continue;
        }
        break;
      case kInstJumpFalse:
        if (!read0()) {
          ip = read1();
          return Continue;
        }
        break;
      case kInstLess:
        write2(read0() < read1());
        break;
      case kInstEqual:
        write2(read0() == read1());
        break;
      case kInstBase:
        base += read0();
        break;
      case kInstExit:
        return Finished;
    }
    ip += OpSize(opcode);
    return Continue;
  }

  State Run(void) {
    State state = Continue;
    while (state == Continue) {
      state = Step();
    };
    return state;
  }

  static size_t OpSize(const OpCode &opcode) {
    int op = std::get<0>(opcode);
    switch (op) {
      case kInstAdd:
      case kInstMul:
        return 4;
      case kInstIn:
      case kInstOut:
        return 2;
      case kInstJumpTrue:
      case kInstJumpFalse:
        return 3;
      case kInstLess:
      case kInstEqual:
        return 4;
      case kInstBase:
        return 2;
      case kInstExit:
        return 1;
    }
    throw std::runtime_error("No op " + std::to_string(op));
  };

  std::deque<basetype> code{};
  std::deque<basetype> inputs{};
  std::deque<basetype> outputs{};
};

std::istream &operator>>(std::istream &is, IntCodeMachine &mach) {
  const static std::regex re_item("-?[0-9]+");
  mach.code.clear();
  std::string buffer{};
  is >> buffer;
  for (auto i{std::sregex_iterator(buffer.begin(), buffer.end(), re_item)};
      i != std::sregex_iterator(); ++i) {
    mach.code.emplace_back(std::stol(i->str()));
  }
  return is;
}

int main(int argc, const char *argv[]) {
  assert(argc == 2);
  IntCodeMachine machine{};
  {
    std::ifstream codefile(argv[1]);
    if (!codefile.is_open()) {
      throw std::runtime_error("File not opened");
    }
    codefile >> machine;
  }

  auto machine0 = machine;
  auto machine1 = machine;

  machine0.Run();

  std::vector<std::vector<char>> world(1, std::vector<char>{});

  for (const auto &v : machine0.outputs) {
    std::cout << static_cast<char>(v);
    switch (v) {
      case '\n':
        world.push_back(std::vector<char>{});
        break;
      case '#':
      case '.':
      case 'v':
      case '<':
      case '>':
      case '^':
        world.back().push_back(v);
        break;
    }
  }

  world.pop_back();
  world.pop_back();

  int ysz = world.size();
  int xsz = world[0].size();

  using BotLoc = std::pair<int, int>;
  using BotDir = int;
  using TurnWalk = std::pair<int, int>;
  using TurnWalks = std::deque<TurnWalk>;

  BotLoc botloc{};
  BotDir botdir{};

  int sumalign{0};
  for (int iy{1}; iy < ysz - 1; ++iy) {
    for (int ix{1}; ix < xsz - 1; ++ix) {
      if (world[iy][ix] == '#' && world[iy - 1][ix] == '#'
          && world[iy + 1][ix] == '#' && world[iy][ix - 1] == '#'
          && world[iy][ix + 1] == '#')
        sumalign += iy * ix;
    }
  }

  for (int iy{0}; iy < ysz; ++iy) {
    for (int ix{0}; ix < xsz; ++ix) {
      auto v = world[iy][ix];
      if (v == '^') {
        botloc = {ix, iy};
        botdir = 0;
      }
      if (v == '>') {
        botloc = {ix, iy};
        botdir = 1;
      }
      if (v == 'v') {
        botloc = {ix, iy};
        botdir = 2;
      }
      if (v == '<') {
        botloc = {ix, iy};
        botdir = 3;
      }
    }
  }

  auto Mov = [](const BotLoc &bl, const BotDir &bd) -> BotLoc {
    switch (bd) {
      default:
      case 0:
        return {bl.first, bl.second - 1};
      case 1:
        return {bl.first + 1, bl.second};
      case 2:
        return {bl.first, bl.second + 1};
      case 3:
        return {bl.first - 1, bl.second};
    }
  };

  auto Left = [](const BotDir &bd) -> BotDir {
    return (bd + 3) % 4;
  };

  auto Right = [](const BotDir &bd) -> BotDir {
    return (bd + 1) % 4;
  };

  auto Stepable = [&xsz, &ysz, &world](const BotLoc &loc) -> bool {
    if (loc.first < 0)
      return false;
    if (loc.second < 0)
      return false;
    if (loc.first >= xsz)
      return false;
    if (loc.second >= ysz)
      return false;
    return world[loc.second][loc.first] == '#';
  };

  auto FindTurn = [&Mov, &Left, &Right, &Stepable](const BotLoc &loc,
      const BotDir &dir) -> int {
    if (Stepable(Mov(loc, Left(dir))))
      return 3;
    if (Stepable(Mov(loc, Right(dir))))
      return 1;
    return -1;
  };

  auto FindWalk = [&Mov, &Stepable](BotLoc &loc, const BotDir &dir) -> int {
    int result{0};
    for (;;) {
      BotLoc tryloc = Mov(loc, dir);
      if (Stepable(tryloc)) {
        result += 1;
        loc = tryloc;
      } else {
        break;
      }
    }
    return result;
  };

  auto FindTurnWalk =
      [&FindTurn, &FindWalk](BotLoc &loc, BotDir &dir) -> TurnWalk {
        int turn = FindTurn(loc, dir);
        if (-1 == turn) {
          return {-1, -1};
        }
        dir = (dir + turn) % 4;
        int steps = FindWalk(loc, dir);
        return {turn, steps};
      };

  TurnWalks w;
  for (TurnWalk v{-2, -2};;) {
    v = FindTurnWalk(botloc, botdir);
    if (v.first == -1 || v.second == -1)
      break;
    w.emplace_back(v);
  };

  machine1.code[0] = 2;

  std::vector<int> seq{};
  TurnWalks ATW{};
  TurnWalks BTW{};
  TurnWalks CTW{};

  for (int alen{1}; alen <= 5; ++alen) {
    std::vector<int> aseq{0};
    auto ait = w.begin();
    TurnWalks ATW_(w.begin(), w.begin() + alen);
    ait += alen;
    while (std::equal(ATW_.begin(), ATW_.end(), ait)) {
      ait += alen;
      aseq.push_back(0);
    }
    for (int blen{1}; blen <= 5; ++blen) {
      auto bseq = aseq;
      auto bit = ait;
      TurnWalks BTW_(bit, bit + blen);
      bit += blen;
      bseq.push_back(1);
      bool bdummy;
      do {
        bdummy = false;
        if (std::equal(ATW_.begin(), ATW_.end(), bit)) {
          bdummy = true;
          bit += alen;
          bseq.push_back(0);
        } else if (std::equal(BTW_.begin(), BTW_.end(), bit)) {
          bdummy = true;
          bit += blen;
          bseq.push_back(1);
        }
      } while (bdummy);
      for (int clen{1}; clen <= 5; ++clen) {
        auto cseq = bseq;
        auto cit = bit;
        TurnWalks CTW_(cit, cit + clen);
        cit += clen;
        cseq.push_back(2);
        bool cdummy;
        do {
          cdummy = false;
          if (std::equal(ATW_.begin(), ATW_.end(), cit)) {
            cdummy = true;
            cit += alen;
            cseq.push_back(0);
          } else if (std::equal(BTW_.begin(), BTW_.end(), cit)) {
            cdummy = true;
            cit += blen;
            cseq.push_back(1);
          } else if (std::equal(CTW_.begin(), CTW_.end(), cit)) {
            cdummy = true;
            cit += clen;
            cseq.push_back(2);
          }
          if (cit == w.end()) {
            seq = cseq;
            ATW = ATW_;
            BTW = BTW_;
            CTW = CTW_;
            goto done;
          }
        } while (cdummy);
      }
    }
  }
  throw std::runtime_error("No compression found\n");
done:

  std::string P{};
  std::string A{};
  std::string B{};
  std::string C{};
  std::string Y{"y\n"};

  for (int i = 0; i < seq.size(); ++i) {
    if (0 != i)
      P += ',';
    P += (seq[i] + 'A');
  }
  P += '\n';

  auto ExtractTW = [](const auto &TW, std::string &target) -> void{
    for (int i = 0; i < TW.size(); ++i) {
      if (0 != i)
        target += ',';
      if (3 == TW[i].first)
        target += "L,";
      else
        target += "R,";
      target += std::to_string(TW[i].second);
    }
    target += '\n';
  };

  ExtractTW(ATW, A);
  ExtractTW(BTW, B);
  ExtractTW(CTW, C);

  std::copy(P.begin(), P.end(), std::back_inserter(machine1.inputs));
  std::copy(A.begin(), A.end(), std::back_inserter(machine1.inputs));
  std::copy(B.begin(), B.end(), std::back_inserter(machine1.inputs));
  std::copy(C.begin(), C.end(), std::back_inserter(machine1.inputs));
  std::copy(Y.begin(), Y.end(), std::back_inserter(machine1.inputs));

  machine1.Run();

  int dust = machine1.outputs.back();
  machine1.outputs.pop_back();

  for (const auto &v : machine1.outputs) {
    std::cout << static_cast<char>(v);
    switch (v) {
      case '\n':
        break;
      default:
        break;
    }
  }

  std::cout << sumalign << " " << dust << "\n";

  return 0;
}