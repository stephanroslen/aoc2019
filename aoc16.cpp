#include <array>
#include <fstream>
#include <iostream>
#include <iterator>
#include <numeric>
#include <string>
#include <vector>

int main(int argc, const char *argv[]) {
  assert(argc == 2);

  std::vector<int> vline;
  {
    std::string line{};
    std::ifstream datafile(argv[1]);
    if (!datafile.is_open()) {
      throw std::runtime_error("File not opened");
    }
    datafile >> line;
    vline.assign(line.size(), 0);
    std::transform(line.begin(),
        line.end(),
        vline.begin(),
        [](char v) -> int { return static_cast<int>(v - '0'); });
  }

  auto vline0 = vline;

  auto gen_pattern = [](int len, int idx) -> std::vector<int> {
    std::vector<int> result(len, 0);
    int i{0};
    std::generate(result.begin(), result.end(), [&i, &idx](void) -> int {
      static const std::array<int, 4> base_pattern{0, 1, 0, -1};
      i += 1;
      return base_pattern[(i / (idx + 1)) % 4];
    });
    return result;
  };

  int len0 = vline0.size();

  std::vector<std::vector<int>> patterns0(len0, std::vector<int>{});
  for (int i{0}; i < len0; ++i) {
    patterns0[i] = gen_pattern(len0, i);
  }

  auto iteration0 =
      [&patterns0](const std::vector<int> &old) -> std::vector<int> {
        size_t sz = old.size();
        std::vector<int> result(sz, 0);
        for (int i{0}; i < sz; ++i) {
          result[i] = std::abs(std::transform_reduce(old.begin(),
              old.end(),
              patterns0[i].begin(),
              0)) % 10;
        }
        return result;
      };

  for (int i{0}; i < 100; ++i)
    vline0 = iteration0(vline0);

  for (size_t i{0}; i < 8; ++i) {
    std::cout << vline0[i];
  }
  std::cout << "\n";

  std::vector<int> vline1(vline.size() * 10000, 0);
  auto dit = vline1.begin();
  for (int i{0}; i < 10000; ++i) {
    dit = std::copy_n(vline.begin(), vline.size(), dit);
  }

  int len1 = vline1.size();

  auto iteration1 = [](std::vector<int> &inplace, size_t sloc) -> void {
    size_t sz = inplace.size();
    std::partial_sum(inplace.rbegin(),
        inplace.rend() - sz + sloc,
        inplace.rbegin());
    std::transform(inplace.rbegin(),
        inplace.rend() - sz + sloc,
        inplace.rbegin(),
        [](int a) -> int { return a % 10; });
  };

  size_t sloc{0};
  for (int i{0}; i < 7; ++i) {
    sloc *= 10;
    sloc += vline[i];
  }

  for (int i{0}; i < 100; ++i)
    iteration1(vline1, sloc);

  for (size_t i{0}; i < 8; ++i) {
    std::cout << vline1[sloc + i];
  }
  std::cout << "\n";
}