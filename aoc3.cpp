#include <algorithm>
#include <deque>
#include <iostream>
#include <iterator>
#include <locale>
#include <map>
#include <regex>
#include <string>
#include <vector>
#include <utility>

using PathSegment = std::pair<char, unsigned int>;
using Path = std::deque<PathSegment>;

Path ExtractPath(const std::string &s) {
  Path result{};
  static const std::regex re_segment("(L|R|U|D)([0-9]+)");
  for (auto i{std::sregex_iterator(s.begin(), s.end(), re_segment)};
      i != std::sregex_iterator(); ++i) {
    result.emplace_back((*i)[1].str()[0], std::stoi((*i)[2].str()));
  }
  return result;
}

using Loc = std::pair<int, int>;
using LocDistMap = std::map<Loc, unsigned int>;

LocDistMap WalkPath(const Path &p) {
  LocDistMap result{};
  Loc current{0, 0};
  unsigned int cdist{0};
  for (auto[dir, dist] : p) {
    for (int i{0}; i < dist; ++i) {
      cdist += 1;
      switch (dir) {
        case 'L':
          current.first -= 1;
          break;
        case 'R':
          current.first += 1;
          break;
        case 'D':
          current.second -= 1;
          break;
        case 'U':
          current.second += 1;
          break;
      }
      result.emplace(current, cdist);
    }
  }
  return result;
}

int main(int, const char *[]) {
  std::string r0{};
  std::string r1{};

  std::cin >> r0 >> r1;

  auto path0 = ExtractPath(r0);
  auto path1 = ExtractPath(r1);

  auto locs0 = WalkPath(path0);
  auto locs1 = WalkPath(path1);

  LocDistMap inter{};
  for (const auto &[l0, d0] : locs0) {
    auto iloc1 = locs1.find(l0);
    if (iloc1 != locs1.end()) {
      auto &d1 = iloc1->second;
      inter.emplace(l0, d0 + d1);
    }
  }

  std::deque<unsigned int> dists0{};
  std::transform(inter.begin(),
      inter.end(),
      std::back_inserter(dists0),
      [](const auto &ld) -> unsigned int {
        const auto &l = ld.first;
        unsigned int res{0};
        res += (l.first < 0) ? -l.first : l.first;
        res += (l.second < 0) ? -l.second : l.second;
        return res;
      });

  std::deque<unsigned int> dists1{};
  std::transform(inter.begin(),
      inter.end(),
      std::back_inserter(dists1),
      [](const auto &ld) -> unsigned int {
        return ld.second;
      });

  std::cout << *std::min_element(dists0.begin(), dists0.end()) << " "
      << *std::min_element(dists1.begin(), dists1.end()) << "\n";

  return 0;
}