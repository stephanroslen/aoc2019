#include <algorithm>
#include <array>
#include <deque>
#include <fstream>
#include <iostream>
#include <iterator>
#include <regex>
#include <vector>

struct IntCodeMachine {
  enum State { Continue, Finished, Wait };

  static constexpr int kInstAdd{1};
  static constexpr int kInstMul{2};
  static constexpr int kInstIn{3};
  static constexpr int kInstOut{4};
  static constexpr int kInstJumpTrue{5};
  static constexpr int kInstJumpFalse{6};
  static constexpr int kInstLess{7};
  static constexpr int kInstEqual{8};
  static constexpr int kInstExit{99};

  IntCodeMachine() = default;
  IntCodeMachine(const IntCodeMachine &) = default;
  IntCodeMachine(IntCodeMachine &&) = default;
  IntCodeMachine &operator=(const IntCodeMachine &) = default;
  IntCodeMachine &operator=(IntCodeMachine &&) = default;
  ~IntCodeMachine() = default;

  int ip{0};

  using OpCode = std::tuple<int, bool, bool, bool>;

  static OpCode DecodeInst(int inst) {
    return {inst % 100, inst / 100 % 2, inst / 1000 % 2, inst / 10000 % 2};
  }

  State Step(void) {
    int inst = code[ip];
    OpCode opcode = DecodeInst(inst);
    auto &[op, mode0, mode1, mode2] = opcode;
    auto access2 = [&](void) -> int {
      int &l = code[ip + 3];
      if (std::get<3>(opcode)) {
        return l;
      } else {
        return code[l];
      }
    };
    auto access1 = [&](void) -> int {
      int &l = code[ip + 2];
      if (std::get<2>(opcode)) {
        return l;
      } else {
        return code[l];
      }
    };
    auto access0 = [&](void) -> int {
      int &l = code[ip + 1];
      if (std::get<1>(opcode)) {
        return l;
      } else {
        return code[l];
      }
    };

    switch (op) {
      case kInstAdd:
        code[code[ip + 3]] = access0() + access1();
        break;
      case kInstMul:
        code[code[ip + 3]] = access0() * access1();
        break;
      case kInstIn:
        if (inputs.size() == 0) {
          return Wait;
        }
        code[code[ip + 1]] = inputs.front();
        inputs.pop_front();
        break;
      case kInstOut:
        outputs.push_back(access0());
        break;
      case kInstJumpTrue:
        if (access0()) {
          ip = access1();
          return Continue;
        }
        break;
      case kInstJumpFalse:
        if (!access0()) {
          ip = access1();
          return Continue;
        }
        break;
      case kInstLess:
        code[code[ip + 3]] = access0() < access1();
        break;
      case kInstEqual:
        code[code[ip + 3]] = access0() == access1();
        break;
      case kInstExit:
        return Finished;
    }
    ip += OpSize(opcode);
    return Continue;
  }

  State Run(void) {
    State state = Continue;
    while (state == Continue) {
      state = Step();
    };
    return state;
  }

  static size_t OpSize(const OpCode &opcode) {
    int op = std::get<0>(opcode);
    switch (op) {
      case kInstAdd:
      case kInstMul:
        return 4;
      case kInstIn:
      case kInstOut:
        return 2;
      case kInstJumpTrue:
      case kInstJumpFalse:
        return 3;
      case kInstLess:
      case kInstEqual:
        return 4;
      case kInstExit:
        return 1;
    }
    throw std::runtime_error("No op " + std::to_string(op));
  };

  std::deque<int> code{};
  std::deque<int> inputs{};
  std::deque<int> outputs{};
};

std::istream &operator>>(std::istream &is, IntCodeMachine &mach) {
  const static std::regex re_item("-?[0-9]+");
  mach.code.clear();
  std::string buffer{};
  is >> buffer;
  for (auto i{std::sregex_iterator(buffer.begin(), buffer.end(), re_item)};
      i != std::sregex_iterator(); ++i) {
    mach.code.emplace_back(std::stoi(i->str()));
  }
  return is;
}

int main(int argc, const char *argv[]) {
  assert(argc == 2);
  IntCodeMachine machine{};
  {
    std::ifstream codefile(argv[1]);
    if (!codefile.is_open()) {
      throw std::runtime_error("File not opened");
    }
    codefile >> machine;
  }

  std::array<int, 5> settings0{0, 1, 2, 3, 4};
  int maxsignal0{0};
  do {
    int signal{0};
    for (const auto phase : settings0) {
      auto cmach = machine;
      cmach.inputs.push_back(phase);
      cmach.inputs.push_back(signal);
      cmach.Run();
      signal = cmach.outputs.front();
    }
    if (signal > maxsignal0) {
      maxsignal0 = signal;
    }
  } while (std::next_permutation(settings0.begin(), settings0.end()));

  std::array<int, 5> settings1{5, 6, 7, 8, 9};
  int maxsignal1{0};
  do {
    std::array<IntCodeMachine, 5>
        machines{machine, machine, machine, machine, machine};
    int signal{0};
    for (int i{0}; i < 5; ++i) {
      machines[i].inputs.push_back(settings1[i]);
    }
    IntCodeMachine::State s = IntCodeMachine::Continue;
    while (s != IntCodeMachine::Finished) {
      for (auto &m : machines) {
        m.inputs.push_back(signal);
        s = m.Run();
        signal = m.outputs.front();
        m.outputs.pop_front();
      }
    }
    if (signal > maxsignal1)
      maxsignal1 = signal;
  } while (std::next_permutation(settings1.begin(), settings1.end()));

  std::cout << maxsignal0 << " " << maxsignal1 << "\n";

  return 0;
}