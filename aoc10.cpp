#include <cmath>
#include <deque>
#include <fstream>
#include <iostream>
#include <iterator>
#include <numeric>
#include <string>
#include <set>
#include <utility>

int main(int argc, const char *argv[]) {
  assert(argc == 2);
  using Asteroid = std::pair<int, int>;
  using Asteroids = std::set<Asteroid>;
  using AsteroidOrder = std::deque<Asteroid>;

  Asteroids asteroids{};
  {
    std::ifstream datafile(argv[1]);
    if (!datafile.is_open()) {
      throw std::runtime_error("File not opened");
    }
    std::string line;
    int iy{0};
    while (datafile >> line) {
      for (int ix{0}; ix < line.size(); ++ix) {
        if (line[ix] == '#')
          asteroids.emplace(ix, iy);
      }
      ++iy;
    }
  }

  auto can_see = [&](const Asteroids &asteroids,
      const Asteroid &a,
      const Asteroid &other) -> bool {
    auto &[xa, ya] = a;
    auto &[xother, yother] = other;
    auto xdiff = xother - xa;
    auto ydiff = yother - ya;
    auto gcd = std::gcd(xdiff, ydiff);
    auto xstep = xdiff / gcd;
    auto ystep = ydiff / gcd;
    int x, y;
    for (x = xa + xstep, y = ya + ystep; x != xother || y != yother;
        x += xstep, y += ystep) {
      if (0 != asteroids.count({x, y})) {
        return false;
      }
    }
    return true;
  };

  int max_cnt{0};

  Asteroid base;

  for (const auto &a : asteroids) {
    int inner_cnt{0};
    for (const auto &other : asteroids) {
      if (a == other)
        continue;
      if (can_see(asteroids, a, other))
        inner_cnt += 1;
    }
    if (inner_cnt > max_cnt) {
      max_cnt = inner_cnt;
      base = a;
    }
  }

  auto destruction_order =
      [&can_see](Asteroids asteroids, Asteroid base) -> AsteroidOrder {
        AsteroidOrder result{};
        asteroids.erase(base);
        while (asteroids.size() > 0) {
          AsteroidOrder visibles{};
          std::copy_if(asteroids.begin(),
              asteroids.end(),
              std::back_inserter(visibles),
              [&can_see, &base, &asteroids](const Asteroid &a) -> bool {
                return can_see(asteroids,
                    base,
                    a);
              });
          for (const auto &v : visibles) {
            asteroids.erase(v);
          }
          std::sort(visibles.begin(),
              visibles.end(),
              [&base](const Asteroid &a, const Asteroid &b) -> bool {
                auto &[xa, ya] = a;
                auto &[xb, yb] = b;
                auto &[xbase, ybase] = base;
                int dxa = xa - xbase;
                int dya = ya - ybase;
                int dxb = xb - xbase;
                int dyb = yb - ybase;

                double anglea = std::atan2(dya, dxa) + (M_PI / 2.0);
                anglea = (anglea < 0.0) ? (anglea + 2 * M_PI) : anglea;
                double angleb = std::atan2(dyb, dxb) + (M_PI / 2.0);
                angleb = (angleb < 0.0) ? (angleb + 2 * M_PI) : angleb;

                return anglea < angleb;
              });
          std::copy(visibles.begin(),
              visibles.end(),
              std::back_inserter(result));
        }
        return result;
      };

  auto order = destruction_order(asteroids, base);
  auto n200 = order[199];

  std::cout << max_cnt << " " << n200.first * 100 + n200.second << "\n";
}