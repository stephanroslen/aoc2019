#include <algorithm>
#include <array>
#include <deque>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <regex>
#include <vector>

struct IntCodeMachine {
  enum State { Continue, Finished, Wait };

  using basetype = int64_t;

  static constexpr int kInstAdd{1};
  static constexpr int kInstMul{2};
  static constexpr int kInstIn{3};
  static constexpr int kInstOut{4};
  static constexpr int kInstJumpTrue{5};
  static constexpr int kInstJumpFalse{6};
  static constexpr int kInstLess{7};
  static constexpr int kInstEqual{8};
  static constexpr int kInstBase{9};
  static constexpr int kInstExit{99};

  IntCodeMachine() = default;
  IntCodeMachine(const IntCodeMachine &) = default;
  IntCodeMachine(IntCodeMachine &&) = default;
  IntCodeMachine &operator=(const IntCodeMachine &) = default;
  IntCodeMachine &operator=(IntCodeMachine &&) = default;
  ~IntCodeMachine() = default;

  basetype ip{0};
  basetype base{0};

  using OpCode = std::tuple<int, int, int, int>;

  static OpCode DecodeInst(int inst) {
    return {inst % 100, inst / 100 % 10, inst / 1000 % 10, inst / 10000 % 10};
  }

  State Step(void) {
    basetype inst = code[ip];
    OpCode opcode = DecodeInst(inst);
    auto &[op, mode0, mode1, mode2] = opcode;

    auto ensure = [&](basetype loc) -> basetype {
      if (code.size() < loc + 1) {
        code.resize(loc + 1, 0);
      }
      return loc;
    };

    auto read2 = [&](void) -> basetype {
      basetype &l = code[ip + 3];
      int mode = std::get<3>(opcode);
      switch (mode) {
        case 0:
          return code[ensure(l)];
        default:
        case 1:
          return l;
        case 2:
          return code[ensure(base + l)];
      }
    };
    auto read1 = [&](void) -> basetype {
      basetype &l = code[ip + 2];
      int mode = std::get<2>(opcode);
      switch (mode) {
        case 0:
          return code[ensure(l)];
        default:
        case 1:
          return l;
        case 2:
          return code[ensure(base + l)];
      }
    };
    auto read0 = [&](void) -> basetype {
      basetype &l = code[ip + 1];
      int mode = std::get<1>(opcode);
      switch (mode) {
        case 0:
          return code[ensure(l)];
        default:
        case 1:
          return l;
        case 2:
          return code[ensure(base + l)];
      }
    };

    auto write2 = [&](basetype val) -> void {
      basetype &l = code[ip + 3];
      int mode = std::get<3>(opcode);
      switch (mode) {
        default:
        case 0:
          code[ensure(l)] = val;
        case 2:
          code[ensure(base + l)] = val;
      }
    };
    auto write1 = [&](basetype val) -> void {
      basetype &l = code[ip + 2];
      int mode = std::get<2>(opcode);
      switch (mode) {
        default:
        case 0:
          code[ensure(l)] = val;
        case 2:
          code[ensure(base + l)] = val;
      }
    };
    auto write0 = [&](basetype val) -> void {
      basetype &l = code[ip + 1];
      int mode = std::get<1>(opcode);
      switch (mode) {
        default:
        case 0:
          code[ensure(l)] = val;
        case 2:
          code[ensure(base + l)] = val;
      }
    };

    switch (op) {
      case kInstAdd:
        write2(read0() + read1());
        break;
      case kInstMul:
        write2(read0() * read1());
        break;
      case kInstIn:
        if (inputs.size() == 0) {
          return Wait;
        }
        write0(inputs.front());
        inputs.pop_front();
        break;
      case kInstOut:
        outputs.push_back(read0());
        break;
      case kInstJumpTrue:
        if (read0()) {
          ip = read1();
          return Continue;
        }
        break;
      case kInstJumpFalse:
        if (!read0()) {
          ip = read1();
          return Continue;
        }
        break;
      case kInstLess:
        write2(read0() < read1());
        break;
      case kInstEqual:
        write2(read0() == read1());
        break;
      case kInstBase:
        base += read0();
        break;
      case kInstExit:
        return Finished;
    }
    ip += OpSize(opcode);
    return Continue;
  }

  State Run(void) {
    State state = Continue;
    while (state == Continue) {
      state = Step();
    };
    return state;
  }

  static size_t OpSize(const OpCode &opcode) {
    int op = std::get<0>(opcode);
    switch (op) {
      case kInstAdd:
      case kInstMul:
        return 4;
      case kInstIn:
      case kInstOut:
        return 2;
      case kInstJumpTrue:
      case kInstJumpFalse:
        return 3;
      case kInstLess:
      case kInstEqual:
        return 4;
      case kInstBase:
        return 2;
      case kInstExit:
        return 1;
    }
    throw std::runtime_error("No op " + std::to_string(op));
  };

  std::deque<basetype> code{};
  std::deque<basetype> inputs{};
  std::deque<basetype> outputs{};
};

std::istream &operator>>(std::istream &is, IntCodeMachine &mach) {
  const static std::regex re_item("-?[0-9]+");
  mach.code.clear();
  std::string buffer{};
  is >> buffer;
  for (auto i{std::sregex_iterator(buffer.begin(), buffer.end(), re_item)};
      i != std::sregex_iterator(); ++i) {
    mach.code.emplace_back(std::stol(i->str()));
  }
  return is;
}

int main(int argc, const char *argv[]) {
  assert(argc == 2);
  IntCodeMachine machine{};
  {
    std::ifstream codefile(argv[1]);
    if (!codefile.is_open()) {
      throw std::runtime_error("File not opened");
    }
    codefile >> machine;
  }

  enum Field { Unknown, Space, Wall, Oxygen };

  using Coord = std::pair<int, int>;
  using MScreen = std::map<Coord, Field>;
  using MSet = std::set<Coord>;
  using MPath = std::deque<Coord>;

  auto machine0 = machine;

  Coord coord0{0, 0};
  MScreen mscreen0{{coord0, Field::Space}};

  auto apply_dir = [](const Coord &c, int direction) -> Coord {
    switch (direction) {
      default:
      case 1:
        return {c.first, c.second + 1};
      case 2:
        return {c.first, c.second - 1};
      case 3:
        return {c.first - 1, c.second};
      case 4:
        return {c.first + 1, c.second};
    }
  };

  auto invert_dir = [](int direction) -> int {
    switch (direction) {
      default:
      case 1:
        return 2;
      case 2:
        return 1;
      case 3:
        return 4;
      case 4:
        return 3;
    }
  };

  auto is_unknown = [](const MScreen &scr, const Coord &c) -> bool {
    auto it = scr.find(c);
    return (it == scr.end() || Field::Unknown == it->second);
  };

  auto is_accessible = [](const MScreen &scr, const Coord &c) -> bool {
    return (1 == scr.count(c)
        && (Field::Space == scr.at(c) || Field::Oxygen == scr.at(c)));
  };

  auto dir_unknown = [&apply_dir, &is_unknown](const MScreen &scr,
      const Coord &c,
      int direction) -> bool {
    auto c_ = apply_dir(c, direction);
    return is_unknown(scr, c_);
  };

  auto has_adj_unknowns =
      [&dir_unknown](const MScreen &scr, const Coord &c) -> bool {
        for (int i{1}; i <= 4; ++i) {
          if (dir_unknown(scr, c, i)) {
            return true;
          }
        }
        return false;
      };

  auto with_adj_unknowns = [&has_adj_unknowns](const MScreen &scr) -> MSet {
    MSet result{};
    for (const auto &[c, v] : scr) {
      if (v != Field::Space && v != Field::Oxygen)
        continue;
      if (has_adj_unknowns(scr, c)) {
        result.insert(c);
      }
    }
    return result;
  };

  auto find_closest_adj_unknowns =
      [&apply_dir, &is_accessible](const MScreen &scr,
          const MSet &vals,
          const Coord &cur) -> Coord {
        MSet vis{cur};
        MPath queue{cur};
        while (!queue.empty()) {
          auto c = queue.front();
          queue.pop_front();
          if (1 == vals.count(c)) {
            return c;
          }
          for (int dir{1}; dir <= 4; ++dir) {
            auto target = apply_dir(c, dir);
            if (!is_accessible(scr, target))
              continue;
            if (0 == vis.count(target)) {
              queue.push_back(target);
              vis.insert(target);
            }
          }
        }
        return {0, 0}; // this must not happen
      };

  auto dir_towards =
      [&apply_dir, &invert_dir, &is_accessible](const MScreen &scr,
          const Coord &from,
          const Coord &to) -> std::pair<int, Coord> {
        MSet vis{to};
        MPath queue{to};
        while (!queue.empty()) {
          auto c = queue.front();
          queue.pop_front();
          for (int dir{1}; dir <= 4; ++dir) {
            auto target = apply_dir(c, dir);
            if (target == from) {
              return {invert_dir(dir), c};
            }
            if (!is_accessible(scr, target))
              continue;
            if (0 == vis.count(target)) {
              queue.push_back(target);
              vis.insert(target);
            }
          }
        }
        return {0, {0, 0}}; // this must not happen
      };

  auto dist =
      [&apply_dir, &invert_dir, &is_accessible](const MScreen &scr,
          const Coord &from,
          const Coord &to) -> int {
        std::map<Coord, int> dists{{to, 0}};
        MSet vis{to};
        MPath queue{to};
        while (!queue.empty()) {
          auto c = queue.front();
          queue.pop_front();
          int dist = dists.at(c);
          for (int dir{1}; dir <= 4; ++dir) {
            auto target = apply_dir(c, dir);
            if (target == from) {
              return dist + 1;
            }
            if (!is_accessible(scr, target))
              continue;
            if (0 == vis.count(target)) {
              queue.push_back(target);
              vis.insert(target);
              dists.emplace(target, dist + 1);
            }
          }
        }
        return 0; // this must not happen
      };

  auto max_dist =
      [&apply_dir, &invert_dir, &is_accessible](const MScreen &scr,
          const Coord &from) -> int {
        std::map<Coord, int> dists{{from, 0}};
        int res{0};
        MSet vis{from};
        MPath queue{from};
        while (!queue.empty()) {
          auto c = queue.front();
          queue.pop_front();
          int dist = dists.at(c);
          if (dist > res)
            res = dist;
          for (int dir{1}; dir <= 4; ++dir) {
            auto target = apply_dir(c, dir);
            if (!is_accessible(scr, target))
              continue;
            if (0 == vis.count(target)) {
              queue.push_back(target);
              vis.insert(target);
              dists.emplace(target, dist + 1);
            }
          }
        }
        return res;
      };

  auto find_oxygen = [](const MScreen &scr) -> Coord {
    auto it = std::find_if(scr.begin(), scr.end(), [](const auto &v)->bool{return v.second == Field::Oxygen;});
    return it->first;
  };

  auto print = [](const MScreen &scr, const Coord &cur) -> void {
    int xmin{0};
    int xmax{0};
    int ymin{0};
    int ymax{0};
    for (const auto &[c, v] : scr) {
      const auto &[x, y] = c;
      if (x < xmin)
        xmin = x;
      if (x > xmax)
        xmax = x;
      if (y < ymin)
        ymin = y;
      if (y > ymax)
        ymax = y;
    }
    std::vector<std::vector<Field>> vmap
        (ymax - ymin + 1, std::vector<Field>(xmax - xmin + 1, Field::Unknown));
    for (const auto &[c, v] : scr) {
      const auto &[x, y] = c;
      vmap[y - ymin][x - xmin] = v;
    }
    for (int y{ymax - ymin}; y >= 0; --y) {
      for (int x{0}; x <= xmax - xmin; ++x) {
        if (x + xmin == cur.first && y + ymin == cur.second) {
          std::cout << 'X';
        } else {
          switch (vmap[y][x]) {
            case Field::Unknown:
              std::cout << ' ';
              break;
            case Field::Space:
              std::cout << '.';
              break;
            case Field::Wall:
              std::cout << '#';
              break;
            case Field::Oxygen:
              std::cout << 'o';
              break;
          }
        }
      }
      std::cout << '\n';
    }
  };

  int num_currrent_exploration_targets{};
  do {
    auto current_exploration_targets = with_adj_unknowns(mscreen0);
    num_currrent_exploration_targets = current_exploration_targets.size();
    Coord target;
    int dir{0};
    for (int i{1}; i <= 4; ++i) {
      target = apply_dir(coord0, i);
      if (is_unknown(mscreen0, target)) {
        dir = i;
        break;
      }
    }
    if (0 == dir) {
      auto tmp_target = find_closest_adj_unknowns(mscreen0,
          current_exploration_targets,
          coord0);
      auto dir_coord = dir_towards(mscreen0, coord0, tmp_target);
      dir = dir_coord.first;
      target = dir_coord.second;
    }
    machine0.inputs.push_back(dir);
    machine0.Run();
    int tres = machine0.outputs.front();
    machine0.outputs.pop_front();
    switch (tres) {
      default:
      case 0:
        mscreen0.emplace(target, Field::Wall);
        break;
      case 1:
        mscreen0.emplace(target, Field::Space);
        coord0 = target;
        break;
      case 2:
        mscreen0.emplace(target, Field::Oxygen);
        coord0 = target;
        break;
    }
  } while (num_currrent_exploration_targets > 0);

  print(mscreen0, coord0);
  std::cout << "\n";

  auto oxygen = find_oxygen(mscreen0);
  auto dist_oxygen = dist(mscreen0, {0,0}, oxygen);
  auto max_dist_from_oxygen = max_dist(mscreen0, oxygen);

  std::cout << dist_oxygen << " " << max_dist_from_oxygen << "\n";

  return 0;
}