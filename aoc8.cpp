#include <array>
#include <deque>
#include <fstream>
#include <iostream>
#include <regex>
#include <vector>

template<size_t W, size_t H>
using Layer_ = std::array<uint8_t, W * H>;

constexpr size_t kWidth{25};
constexpr size_t kHeight{6};

using Layer = Layer_<kWidth, kHeight>;

constexpr size_t kLayerSize = std::tuple_size_v<Layer>;

int main(int argc, const char *argv[]) {
  assert(argc == 2);
  std::deque<uint8_t> figs{};
  {
    std::ifstream datafile(argv[1]);
    if (!datafile.is_open()) {
      throw std::runtime_error("File not opened");
    }
    std::string data((std::istreambuf_iterator<char>(datafile)),
        std::istreambuf_iterator<char>());

    const static std::regex re_fig("[0-9]");
    std::transform(std::sregex_iterator(data.begin(), data.end(), re_fig),
        std::sregex_iterator(),
        std::back_inserter(figs),
        [&](const auto &m) {
          return static_cast<uint8_t>(std::stoi(m.str()));
        });
  }

  size_t layers_cnt = figs.size() / kLayerSize;
  std::vector<Layer> layers(layers_cnt);
  auto it = figs.begin();
  for (auto &layer : layers) {
    std::copy_n(it, kLayerSize, layer.begin());
    std::advance(it, kLayerSize);
  }

  auto &most_zeros = *std::min_element(layers.begin(),
      layers.end(),
      [](const auto &la, const auto &lb) -> bool {
        return std::count(la.begin(), la.end(), 0)
            < std::count(lb.begin(), lb.end(), 0);
      });

  size_t ones = std::count(most_zeros.begin(), most_zeros.end(), 1);
  size_t twos = std::count(most_zeros.begin(), most_zeros.end(), 2);

  size_t result0 = ones * twos;

  std::cout << result0 << "\n";

  Layer decoded;
  std::fill(decoded.begin(), decoded.end(), 2);

  for (const auto &layer : layers) {
    for (size_t i{0}; i < kLayerSize; ++i) {
      if (2 == decoded[i]) {
        decoded[i] = layer[i];
      }
    }
  }

  for (int iy{0}; iy < kHeight; ++iy) {
    for (int ix{0}; ix < kWidth; ++ix) {
      std::cout << ((decoded[ix + iy * kWidth] == 1) ? "#" : " ");
    }
    std::cout << "\n";
  }
}