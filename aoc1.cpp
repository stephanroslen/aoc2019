#include <algorithm>
#include <deque>
#include <iostream>
#include <numeric>
#include <iterator>

int main(int, const char *[]) {
  std::deque<int> masses{};
  std::copy(std::istream_iterator<int>(std::cin),
      std::istream_iterator<int>(),
      std::back_inserter(masses));
  int first = std::transform_reduce(masses.begin(),
      masses.end(),
      0,
      std::plus(),
      [](int v) -> int { return v / 3 - 2; });
  int second = std::transform_reduce(masses.begin(),
      masses.end(),
      0,
      std::plus(),
      [](int v) -> int {
        int accum {0};
        while ((v = v / 3 - 2) > 0) {
          accum += v;
        }
        return accum;
      });
  std::cout << first << " " << second << "\n";
  return 0;
}
