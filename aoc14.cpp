#include <cmath>
#include <deque>
#include <fstream>
#include <iostream>
#include <iterator>
#include <numeric>
#include <string>
#include <set>
#include <map>
#include <regex>
#include <utility>

template<class Key, class T, class Compare, class Alloc, class Pred>
void erase_if(std::map<Key, T, Compare, Alloc> &c, Pred pred) {
  for (auto i = c.begin(), last = c.end(); i != last;) {
    if (pred(*i)) {
      i = c.erase(i);
    } else {
      ++i;
    }
  }
}

int main(int argc, const char *argv[]) {
  assert(argc == 2);
  using Chem = std::string;
  using Component = std::pair<long int, Chem>;
  using Components = std::deque<Component>;
  using RecipeDetails = std::pair<long int, Components>;
  using Recipes = std::map<Chem, RecipeDetails>;
  using Process = std::map<Chem, long int>;

  Recipes recipes{};
  {
    std::ifstream datafile(argv[1]);
    if (!datafile.is_open()) {
      throw std::runtime_error("File not opened");
    }
    std::string buffer{std::istreambuf_iterator<char>(datafile),
        std::istreambuf_iterator<char>()};
    static const std::regex re_recipe("(.+) => ([0-9]+) ([A-Z]+)");
    std::transform(std::sregex_iterator(buffer.begin(),
        buffer.end(),
        re_recipe),
        std::sregex_iterator(),
        std::inserter(recipes, recipes.end()),
        [&](const auto &m_recipe) -> Recipes::value_type {
          static const std::regex re_component("([0-9]+) ([A-Z]+)");
          Recipes::value_type result{m_recipe[3].str(), {}};
          auto &[rname, rrest] = result;
          auto &[ramount, rcomps] = rrest;
          auto components = m_recipe[1].str();
          ramount = std::stoi(m_recipe[2].str());
          std::transform(std::sregex_iterator(components.begin(),
              components.end(),
              re_component),
              std::sregex_iterator(),
              std::back_inserter(rcomps),
              [&](const auto &m_component) -> Component {
                return Component{std::stoi(m_component[1].str()),
                    m_component[2].str()};
              });
          return result;
        });
  }


  auto calc_ore = [&recipes](long int v)->long int {
    Process process{{"FUEL", v}};
    Process reserve{};

    while (1 != process.size() || process.begin()->first != "ORE") {
      auto prodit = std::find_if_not(process.begin(),
          process.end(),
          [](const auto &e) -> bool {
            return e.first == "ORE";
          });
      auto[prodi, proda] = *prodit;
      process.erase(prodit);

      auto &ireserve = reserve[prodi];
      long int ineed = proda - ireserve;
      if (ineed <= 0) {
        ireserve -= proda;
      } else {
        auto &rentry = recipes.at(prodi);
        auto &[ramount, rcomps] = rentry;

        long int rcnt = ineed / ramount;
        if (rcnt * ramount < ineed)
          rcnt += 1;

        long int iexc = rcnt * ramount - ineed;

        ireserve = iexc;

        for (const auto &[ca, ci] : rcomps) {
          auto &pp = process[ci];
          long int add = ca * rcnt;
          pp += add;
        }
      }
      if (ireserve == 0) {
        reserve.erase(prodi);
      }
    }

    return process.begin()->second;
  };

  auto ore0 = calc_ore(1);
  long int ore_target = 1000000000000;
  long int fuel_max_val{ore_target / ore0};
  long int fuel_min_exc{2 * fuel_max_val};

  while (fuel_min_exc - fuel_max_val > 1) {
    long int fuel_target = (fuel_min_exc + fuel_max_val) / 2;
    long int ore = calc_ore(fuel_target);
    if (ore > ore_target) {
      fuel_min_exc = fuel_target;
    } else if (ore < ore_target) {
      fuel_max_val = fuel_target;
    }
  }

  std::cout << ore0 << " " << fuel_max_val << "\n";
}