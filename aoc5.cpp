#include <algorithm>
#include <deque>
#include <fstream>
#include <iostream>
#include <iterator>
#include <regex>
#include <vector>

struct IntCodeMachine {
  static constexpr int kInstAdd{1};
  static constexpr int kInstMul{2};
  static constexpr int kInstIn{3};
  static constexpr int kInstOut{4};
  static constexpr int kInstJumpTrue{5};
  static constexpr int kInstJumpFalse{6};
  static constexpr int kInstLess{7};
  static constexpr int kInstEqual{8};
  static constexpr int kInstExit{99};

  IntCodeMachine() = default;
  IntCodeMachine(const IntCodeMachine &) = default;
  IntCodeMachine(IntCodeMachine &&) = default;
  IntCodeMachine &operator=(const IntCodeMachine &) = default;
  IntCodeMachine &operator=(IntCodeMachine &&) = default;
  ~IntCodeMachine() = default;

  int ip{0};

  using OpCode = std::tuple<int, bool, bool, bool>;

  static OpCode DecodeInst(int inst) {
    return {inst % 100, inst / 100 % 2, inst / 1000 % 2, inst / 10000 % 2};
  }

  bool Step(void) {
    int inst = code[ip];
    OpCode opcode = DecodeInst(inst);
    auto &[op, mode0, mode1, mode2] = opcode;
    auto access2 = [&](void) -> int {
      int &l = code[ip + 3];
      if (std::get<3>(opcode)) {
        return l;
      } else {
        return code[l];
      }
    };
    auto access1 = [&](void) -> int {
      int &l = code[ip + 2];
      if (std::get<2>(opcode)) {
        return l;
      } else {
        return code[l];
      }
    };
    auto access0 = [&](void) -> int {
      int &l = code[ip + 1];
      if (std::get<1>(opcode)) {
        return l;
      } else {
        return code[l];
      }
    };

    switch (op) {
      case kInstAdd:
        code[code[ip + 3]] = access0() + access1();
        break;
      case kInstMul:
        code[code[ip + 3]] = access0() * access1();
        break;
      case kInstIn:
        std::cout << "> ";
        std::cin >> code[code[ip + 1]];
        break;
      case kInstOut:
        std::cout << access0() << "\n";
        break;
      case kInstJumpTrue:
        if (access0()) {
          ip = access1();
          return true;
        }
        break;
      case kInstJumpFalse:
        if (!access0()) {
          ip = access1();
          return true;
        }
        break;
      case kInstLess:
        code[code[ip + 3]] = access0() < access1();
        break;
      case kInstEqual:
        code[code[ip + 3]] = access0() == access1();
        break;
      case kInstExit:
        return false;
    }
    ip += OpSize(opcode);
    return true;
  }

  void Run(void) {
    while (Step()) {};
  }

  static size_t OpSize(const OpCode &opcode) {
    int op = std::get<0>(opcode);
    switch (op) {
      case kInstAdd:
      case kInstMul:
        return 4;
      case kInstIn:
      case kInstOut:
        return 2;
      case kInstJumpTrue:
      case kInstJumpFalse:
        return 3;
      case kInstLess:
      case kInstEqual:
        return 4;
      case kInstExit:
        return 1;
    }
    throw std::runtime_error("No op " + std::to_string(op));
  };

  std::deque<int> code{};
};

std::istream &operator>>(std::istream &is, IntCodeMachine &mach) {
  const static std::regex re_item("-?[0-9]+");
  mach.code.clear();
  std::string buffer{};
  is >> buffer;
  for (auto i{std::sregex_iterator(buffer.begin(), buffer.end(), re_item)};
      i != std::sregex_iterator(); ++i) {
    mach.code.emplace_back(std::stoi(i->str()));
  }
  return is;
}

int main(int argc, const char *argv[]) {
  assert(argc == 2);
  IntCodeMachine machine0{};
  {
    std::ifstream codefile(argv[1]);
    if (!codefile.is_open()) {
      throw std::runtime_error("File not opened");
    }
    codefile >> machine0;
  }

  machine0.Run();

  return 0;
}