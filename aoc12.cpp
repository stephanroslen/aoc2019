#include <cmath>
#include <deque>
#include <fstream>
#include <iostream>
#include <iterator>
#include <numeric>
#include <regex>
#include <set>
#include <string>
#include <set>
#include <utility>

struct Pos3 {
  int x{0};
  int y{0};
  int z{0};
};

using Moon = std::pair<Pos3, Pos3>;
using Moons = std::deque<Moon>;

std::istream &operator>>(std::istream &is, Moons &moons) {

  moons.clear();
  std::string buffer{};
  is >> buffer;

  return is;
}

int main(int argc, const char *argv[]) {
  assert(argc == 2);

  Moons moons{};

  {
    std::ifstream datafile(argv[1]);
    if (!datafile.is_open()) {
      throw std::runtime_error("File not opened");
    }
    std::string buffer{std::istreambuf_iterator<char>(datafile),
        std::istreambuf_iterator<char>()};
    const static std::regex
        re_item("<x=(-?[0-9]+), y=(-?[0-9]+), z=(-?[0-9]+)>");
    std::for_each(std::sregex_iterator(buffer.begin(), buffer.end(), re_item),
        std::sregex_iterator(),
        [&](const auto &m) {
          moons.emplace_back(
              Pos3{std::stoi(m[1].str()),
                  std::stoi(m[2].str()),
                  std::stoi(m[3].str())},
              Pos3{0, 0, 0});
        });
  }

  auto startmoons = moons;

  unsigned long int found_equal_x{0};
  unsigned long int found_equal_y{0};
  unsigned long int found_equal_z{0};

  Moons moons1000;

  for (unsigned long int i{0}; true; ++i) {
    if (i == 1000)
      moons1000 = moons;
    std::for_each(moons.begin(), moons.end(), [&](Moon &a) {
      std::for_each(moons.begin(), moons.end(), [&](Moon &b) {
        if (&a != &b) {
          auto &[apos, avel] = a;
          auto &[bpos, bvel] = b;
          if (apos.x < bpos.x) {
            avel.x += 1;
          } else if (apos.x > bpos.x) {
            avel.x -= 1;
          }
          if (apos.y < bpos.y) {
            avel.y += 1;
          } else if (apos.y > bpos.y) {
            avel.y -= 1;
          }
          if (apos.z < bpos.z) {
            avel.z += 1;
          } else if (apos.z > bpos.z) {
            avel.z -= 1;
          }
        }
      });
    });

    std::for_each(moons.begin(), moons.end(), [&](Moon &m) {
      auto &[mpos, mvel] = m;
      mpos.x += mvel.x;
      mpos.y += mvel.y;
      mpos.z += mvel.z;
    });

    bool xeq{true};
    bool yeq{true};
    bool zeq{true};
    for (int im{0}; im < moons.size(); ++im) {
      auto &[mpos, mvel] = moons[im];
      auto &[zpos, zvel] = startmoons[im];
      if (mpos.x != zpos.x)
        xeq = false;
      if (mvel.x != zvel.x)
        xeq = false;
      if (mpos.y != zpos.y)
        yeq = false;
      if (mvel.y != zvel.y)
        yeq = false;
      if (mpos.z != zpos.z)
        zeq = false;
      if (mvel.z != zvel.z)
        zeq = false;
    }
    if (found_equal_x == 0 && xeq) {
      found_equal_x = i + 1;
    }
    if (found_equal_y == 0 && yeq) {
      found_equal_y = i + 1;
    }
    if (found_equal_z == 0 && zeq) {
      found_equal_z = i + 1;
    }

    if (found_equal_x && found_equal_y && found_equal_z)
      break;
  }

  auto energy = [](const Moon &m) -> int {
    auto &[mpos, mvel] = m;
    return (abs(mpos.x) + abs(mpos.y) + abs(mpos.z))
        * (abs(mvel.x) + abs(mvel.y) + abs(mvel.z));
  };

  int res0 =
      std::transform_reduce(moons1000.begin(),
          moons1000.end(),
          0,
          std::plus(),
          energy);

  unsigned long int
      res1 = std::lcm(found_equal_x, std::lcm(found_equal_y, found_equal_z));

  std::cout << res0 << " " << res1 << "\n";
}