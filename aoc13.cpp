#include <algorithm>
#include <array>
#include <deque>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <regex>
#include <vector>

struct IntCodeMachine {
  enum State { Continue, Finished, Wait };

  using basetype = int64_t;

  static constexpr int kInstAdd{1};
  static constexpr int kInstMul{2};
  static constexpr int kInstIn{3};
  static constexpr int kInstOut{4};
  static constexpr int kInstJumpTrue{5};
  static constexpr int kInstJumpFalse{6};
  static constexpr int kInstLess{7};
  static constexpr int kInstEqual{8};
  static constexpr int kInstBase{9};
  static constexpr int kInstExit{99};

  IntCodeMachine() = default;
  IntCodeMachine(const IntCodeMachine &) = default;
  IntCodeMachine(IntCodeMachine &&) = default;
  IntCodeMachine &operator=(const IntCodeMachine &) = default;
  IntCodeMachine &operator=(IntCodeMachine &&) = default;
  ~IntCodeMachine() = default;

  basetype ip{0};
  basetype base{0};

  using OpCode = std::tuple<int, int, int, int>;

  static OpCode DecodeInst(int inst) {
    return {inst % 100, inst / 100 % 10, inst / 1000 % 10, inst / 10000 % 10};
  }

  State Step(void) {
    basetype inst = code[ip];
    OpCode opcode = DecodeInst(inst);
    auto &[op, mode0, mode1, mode2] = opcode;

    auto ensure = [&](basetype loc) -> basetype {
      if (code.size() < loc + 1) {
        code.resize(loc + 1, 0);
      }
      return loc;
    };

    auto read2 = [&](void) -> basetype {
      basetype &l = code[ip + 3];
      int mode = std::get<3>(opcode);
      switch (mode) {
        case 0:
          return code[ensure(l)];
        default:
        case 1:
          return l;
        case 2:
          return code[ensure(base + l)];
      }
    };
    auto read1 = [&](void) -> basetype {
      basetype &l = code[ip + 2];
      int mode = std::get<2>(opcode);
      switch (mode) {
        case 0:
          return code[ensure(l)];
        default:
        case 1:
          return l;
        case 2:
          return code[ensure(base + l)];
      }
    };
    auto read0 = [&](void) -> basetype {
      basetype &l = code[ip + 1];
      int mode = std::get<1>(opcode);
      switch (mode) {
        case 0:
          return code[ensure(l)];
        default:
        case 1:
          return l;
        case 2:
          return code[ensure(base + l)];
      }
    };

    auto write2 = [&](basetype val) -> void {
      basetype &l = code[ip + 3];
      int mode = std::get<3>(opcode);
      switch (mode) {
        default:
        case 0:
          code[ensure(l)] = val;
        case 2:
          code[ensure(base + l)] = val;
      }
    };
    auto write1 = [&](basetype val) -> void {
      basetype &l = code[ip + 2];
      int mode = std::get<2>(opcode);
      switch (mode) {
        default:
        case 0:
          code[ensure(l)] = val;
        case 2:
          code[ensure(base + l)] = val;
      }
    };
    auto write0 = [&](basetype val) -> void {
      basetype &l = code[ip + 1];
      int mode = std::get<1>(opcode);
      switch (mode) {
        default:
        case 0:
          code[ensure(l)] = val;
        case 2:
          code[ensure(base + l)] = val;
      }
    };

    switch (op) {
      case kInstAdd:
        write2(read0() + read1());
        break;
      case kInstMul:
        write2(read0() * read1());
        break;
      case kInstIn:
        if (inputs.size() == 0) {
          return Wait;
        }
        write0(inputs.front());
        inputs.pop_front();
        break;
      case kInstOut:
        outputs.push_back(read0());
        break;
      case kInstJumpTrue:
        if (read0()) {
          ip = read1();
          return Continue;
        }
        break;
      case kInstJumpFalse:
        if (!read0()) {
          ip = read1();
          return Continue;
        }
        break;
      case kInstLess:
        write2(read0() < read1());
        break;
      case kInstEqual:
        write2(read0() == read1());
        break;
      case kInstBase:
        base += read0();
        break;
      case kInstExit:
        return Finished;
    }
    ip += OpSize(opcode);
    return Continue;
  }

  State Run(void) {
    State state = Continue;
    while (state == Continue) {
      state = Step();
    };
    return state;
  }

  static size_t OpSize(const OpCode &opcode) {
    int op = std::get<0>(opcode);
    switch (op) {
      case kInstAdd:
      case kInstMul:
        return 4;
      case kInstIn:
      case kInstOut:
        return 2;
      case kInstJumpTrue:
      case kInstJumpFalse:
        return 3;
      case kInstLess:
      case kInstEqual:
        return 4;
      case kInstBase:
        return 2;
      case kInstExit:
        return 1;
    }
    throw std::runtime_error("No op " + std::to_string(op));
  };

  std::deque<basetype> code{};
  std::deque<basetype> inputs{};
  std::deque<basetype> outputs{};
};

std::istream &operator>>(std::istream &is, IntCodeMachine &mach) {
  const static std::regex re_item("-?[0-9]+");
  mach.code.clear();
  std::string buffer{};
  is >> buffer;
  for (auto i{std::sregex_iterator(buffer.begin(), buffer.end(), re_item)};
      i != std::sregex_iterator(); ++i) {
    mach.code.emplace_back(std::stol(i->str()));
  }
  return is;
}

int main(int argc, const char *argv[]) {
  assert(argc == 2);
  IntCodeMachine machine{};
  {
    std::ifstream codefile(argv[1]);
    if (!codefile.is_open()) {
      throw std::runtime_error("File not opened");
    }
    codefile >> machine;
  }

  using Coord = std::pair<int, int>;
  using MScreen = std::map<Coord, int>;

  auto machine0 = machine;

  MScreen mscreen0{};

  auto s0 = machine0.Run();

  int xmin{0}, xmax{0};
  int ymin{0}, ymax{0};

  while (!machine0.outputs.empty()) {
    int x = machine0.outputs.front();
    machine0.outputs.pop_front();
    int y = machine0.outputs.front();
    machine0.outputs.pop_front();
    int i = machine0.outputs.front();
    machine0.outputs.pop_front();
    if (x < xmin)
      xmin = x;
    if (x > xmax)
      xmax = x;
    if (y < ymin)
      ymin = y;
    if (y > ymax)
      ymax = y;
    mscreen0.emplace(std::pair{x, y}, i);
  }

  int blocks = std::count_if(mscreen0.begin(),
      mscreen0.end(),
      [](const auto &a) -> bool {
        return a.second == 2;
      });

  auto machine1 = machine;

  using VScreenLine = std::vector<int>;
  using VScreen = std::vector<VScreenLine>;
  int score{};

  VScreen screen1(ymax - ymin + 1, VScreenLine(xmax - xmin + 1, 0));

  machine1.code[0] = 2;

  int xball{};
  int yball{};
  int xpaddle{};
  int ypaddle{};
  IntCodeMachine::State s1;
  do {
    s1 = machine1.Run();
    while (!machine1.outputs.empty()) {
      int x = machine1.outputs.front();
      machine1.outputs.pop_front();
      int y = machine1.outputs.front();
      machine1.outputs.pop_front();
      int i = machine1.outputs.front();
      machine1.outputs.pop_front();
      if (x == -1 && y == 0) {
        score = i;
      } else {
        screen1[y - ymin][x - xmin] = i;
        if (i == 4) {
          xball = x;
          yball = y;
        } else if (i == 3) {
          xpaddle = x;
          ypaddle = y;
        }
      }
    }
    if (xball > xpaddle)
      machine1.inputs.push_back(1);
    else if (xball < xpaddle)
      machine1.inputs.push_back(-1);
    else
      machine1.inputs.push_back(0);

    if constexpr(false) {
      for (int iy{ymin}; iy <= ymax; ++iy) {
        for (int ix{xmin}; ix <= xmax; ++ix) {
          char output;
          int t = screen1[iy][ix];
          switch (t) {
            case 1:
              output = '#';
              break;
            case 2:
              output = 'B';
              break;
            case 3:
              output = '-';
              break;
            case 4:
              output = 'o';
              break;
            default:
              output = ' ';
              break;
          }
          std::cout << output;
        }
        std::cout << "\n";
      }
      std::cout << xball << "," << yball << " " << xpaddle << "," << ypaddle
          << " " << score << "\n";
    }
  } while (s1 != IntCodeMachine::Finished);

  std::cout << blocks << " " << score << "\n";

  return 0;
}