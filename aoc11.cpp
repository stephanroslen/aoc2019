#include <algorithm>
#include <array>
#include <deque>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <regex>
#include <vector>

struct IntCodeMachine {
  enum State { Continue, Finished, Wait };

  using basetype = int64_t;

  static constexpr int kInstAdd{1};
  static constexpr int kInstMul{2};
  static constexpr int kInstIn{3};
  static constexpr int kInstOut{4};
  static constexpr int kInstJumpTrue{5};
  static constexpr int kInstJumpFalse{6};
  static constexpr int kInstLess{7};
  static constexpr int kInstEqual{8};
  static constexpr int kInstBase{9};
  static constexpr int kInstExit{99};

  IntCodeMachine() = default;
  IntCodeMachine(const IntCodeMachine &) = default;
  IntCodeMachine(IntCodeMachine &&) = default;
  IntCodeMachine &operator=(const IntCodeMachine &) = default;
  IntCodeMachine &operator=(IntCodeMachine &&) = default;
  ~IntCodeMachine() = default;

  basetype ip{0};
  basetype base{0};

  using OpCode = std::tuple<int, int, int, int>;

  static OpCode DecodeInst(int inst) {
    return {inst % 100, inst / 100 % 10, inst / 1000 % 10, inst / 10000 % 10};
  }

  State Step(void) {
    basetype inst = code[ip];
    OpCode opcode = DecodeInst(inst);
    auto &[op, mode0, mode1, mode2] = opcode;

    auto ensure = [&](basetype loc) -> basetype {
      if (code.size() < loc + 1) {
        code.resize(loc + 1, 0);
      }
      return loc;
    };

    auto read2 = [&](void) -> basetype {
      basetype &l = code[ip + 3];
      int mode = std::get<3>(opcode);
      switch (mode) {
        case 0:
          return code[ensure(l)];
        default:
        case 1:
          return l;
        case 2:
          return code[ensure(base + l)];
      }
    };
    auto read1 = [&](void) -> basetype {
      basetype &l = code[ip + 2];
      int mode = std::get<2>(opcode);
      switch (mode) {
        case 0:
          return code[ensure(l)];
        default:
        case 1:
          return l;
        case 2:
          return code[ensure(base + l)];
      }
    };
    auto read0 = [&](void) -> basetype {
      basetype &l = code[ip + 1];
      int mode = std::get<1>(opcode);
      switch (mode) {
        case 0:
          return code[ensure(l)];
        default:
        case 1:
          return l;
        case 2:
          return code[ensure(base + l)];
      }
    };

    auto write2 = [&](basetype val) -> void {
      basetype &l = code[ip + 3];
      int mode = std::get<3>(opcode);
      switch (mode) {
        default:
        case 0:
          code[ensure(l)] = val;
        case 2:
          code[ensure(base + l)] = val;
      }
    };
    auto write1 = [&](basetype val) -> void {
      basetype &l = code[ip + 2];
      int mode = std::get<2>(opcode);
      switch (mode) {
        default:
        case 0:
          code[ensure(l)] = val;
        case 2:
          code[ensure(base + l)] = val;
      }
    };
    auto write0 = [&](basetype val) -> void {
      basetype &l = code[ip + 1];
      int mode = std::get<1>(opcode);
      switch (mode) {
        default:
        case 0:
          code[ensure(l)] = val;
        case 2:
          code[ensure(base + l)] = val;
      }
    };

    switch (op) {
      case kInstAdd:
        write2(read0() + read1());
        break;
      case kInstMul:
        write2(read0() * read1());
        break;
      case kInstIn:
        if (inputs.size() == 0) {
          return Wait;
        }
        write0(inputs.front());
        inputs.pop_front();
        break;
      case kInstOut:
        outputs.push_back(read0());
        break;
      case kInstJumpTrue:
        if (read0()) {
          ip = read1();
          return Continue;
        }
        break;
      case kInstJumpFalse:
        if (!read0()) {
          ip = read1();
          return Continue;
        }
        break;
      case kInstLess:
        write2(read0() < read1());
        break;
      case kInstEqual:
        write2(read0() == read1());
        break;
      case kInstBase:
        base += read0();
        break;
      case kInstExit:
        return Finished;
    }
    ip += OpSize(opcode);
    return Continue;
  }

  State Run(void) {
    State state = Continue;
    while (state == Continue) {
      state = Step();
    };
    return state;
  }

  static size_t OpSize(const OpCode &opcode) {
    int op = std::get<0>(opcode);
    switch (op) {
      case kInstAdd:
      case kInstMul:
        return 4;
      case kInstIn:
      case kInstOut:
        return 2;
      case kInstJumpTrue:
      case kInstJumpFalse:
        return 3;
      case kInstLess:
      case kInstEqual:
        return 4;
      case kInstBase:
        return 2;
      case kInstExit:
        return 1;
    }
    throw std::runtime_error("No op " + std::to_string(op));
  };

  std::deque<basetype> code{};
  std::deque<basetype> inputs{};
  std::deque<basetype> outputs{};
};

std::istream &operator>>(std::istream &is, IntCodeMachine &mach) {
  const static std::regex re_item("-?[0-9]+");
  mach.code.clear();
  std::string buffer{};
  is >> buffer;
  for (auto i{std::sregex_iterator(buffer.begin(), buffer.end(), re_item)};
      i != std::sregex_iterator(); ++i) {
    mach.code.emplace_back(std::stol(i->str()));
  }
  return is;
}

int main(int argc, const char *argv[]) {
  assert(argc == 2);
  IntCodeMachine machine{};
  {
    std::ifstream codefile(argv[1]);
    if (!codefile.is_open()) {
      throw std::runtime_error("File not opened");
    }
    codefile >> machine;
  }

  using Location = std::pair<int, int>;
  using World = std::map<Location, int>;
  using Locations = std::set<Location>;
  enum Direction {Up = 0, Left = 1, Down = 2, Right = 3};

  World world0{};
  Location location0{0, 0};
  Direction direction0{Up};
  Locations locations0{};

  auto machine0 = machine;
  IntCodeMachine::State s0 = IntCodeMachine::Continue;

  do {
    locations0.insert(location0);
    auto &cloc = world0[location0];
    machine0.inputs.push_back(cloc);
    s0 = machine0.Run();
    int color = machine0.outputs.front(); machine0.outputs.pop_front();
    int rdir = machine0.outputs.front(); machine0.outputs.pop_front();
    cloc = color;
    if (rdir == 0) {
      direction0 = static_cast<Direction>((static_cast<int>(direction0) + 1) % 4);
    } else {
      direction0 = static_cast<Direction>((static_cast<int>(direction0) + 3) % 4);
    }
    switch (direction0) {
      case Up:
        location0.second += 1;
        break;
      case Left:
        location0.first -= 1;
        break;
      case Down:
        location0.second -= 1;
        break;
      case Right:
        location0.first += 1;
        break;
    }
  } while (IntCodeMachine::Finished != s0);

  std::cout << locations0.size() << "\n";

  World world1{{{0,0}, 1}};
  Location location1{0, 0};
  Direction direction1{Up};

  auto machine1 = machine;
  IntCodeMachine::State s1 = IntCodeMachine::Continue;

  int xmin = 0;
  int xmax = 0;
  int ymin = 0;
  int ymax = 0;

  do {
    auto &[x, y] = location1;
    if (x < xmin)
      xmin = x;
    if (x > xmax)
      xmax = x;
    if (y < ymin)
      ymin = y;
    if (y > ymax)
      ymax = y;
    auto &cloc = world1[location1];
    machine1.inputs.push_back(cloc);
    s1 = machine1.Run();
    int color = machine1.outputs.front(); machine1.outputs.pop_front();
    int rdir = machine1.outputs.front(); machine1.outputs.pop_front();
    cloc = color;
    if (rdir == 0) {
      direction1 = static_cast<Direction>((static_cast<int>(direction1) + 1) % 4);
    } else {
      direction1 = static_cast<Direction>((static_cast<int>(direction1) + 3) % 4);
    }
    switch (direction1) {
      case Up:
        location1.second += 1;
        break;
      case Left:
        location1.first -= 1;
        break;
      case Down:
        location1.second -= 1;
        break;
      case Right:
        location1.first += 1;
        break;
    }
  } while (IntCodeMachine::Finished != s1);

  for (int iy{ymax}; iy >= ymin; --iy) {
    for (int ix{xmin}; ix <= xmax; ++ix ) {
      std::cout << (world1[{ix, iy}] ? '#' : ' ');
    }
    std::cout << "\n";
  }

  return 0;
}