#include <array>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <numeric>
#include <unordered_map>
#include <vector>
#include <utility>

int main(int argc, const char *argv[]) {
  assert(argc == 2);

  using Maze = std::vector<char>;
  using Coord = std::pair<int, int>;
  using Coords = std::array<int, 53>;
  using DistMatrix = std::array<std::array<int, 27>, 27>;
  using KeySet = std::bitset<26>;
  using LockedBy = std::array<KeySet, 26>;
  Maze maze{};
  int linelen{0};
  {
    std::string line{};
    std::string all_lines{};
    std::ifstream datafile(argv[1]);
    if (!datafile.is_open()) {
      throw std::runtime_error("File not opened");
    }
    while (datafile >> line) {
      linelen = line.size();
      all_lines += line;
    }
    std::copy(all_lines.begin(), all_lines.end(), std::back_inserter(maze));
  }

  int lines = maze.size() / linelen;

  auto Coord2Idx = [&linelen](const Coord &c) -> int {
    return c.first + linelen * c.second;
  };

  auto Idx2Coord = [&linelen](int i) -> Coord {
    return {i % linelen, i / linelen};
  };

  auto IsWall = [&Coord2Idx](const Maze &m, const auto &c) -> bool {
    if constexpr (std::is_same_v<decltype(c), const int &>) {
      return '#' == m[c];
    } else {
      return '#' == m[Coord2Idx(c)];
    }
  };

  auto IsFreeSpace = [&Coord2Idx](const Maze &m, const auto &c) -> bool {
    if constexpr (std::is_same_v<decltype(c), const int &>) {
      return '.' == m[c];
    } else {
      return '.' == m[Coord2Idx(c)];
    }
  };

  auto IsDoor = [&Coord2Idx](const Maze &m, const auto &c) -> bool {
    if constexpr (std::is_same_v<decltype(c), const int &>) {
      return isupper(m[c]);
    } else {
      return isupper(m[Coord2Idx(c)]);
    }
  };

  auto IsWalkable = [&Coord2Idx](const Maze &m, const auto &c) -> bool {
    auto Accepted = [](char v) {
      if ('#' == v)
        return false;
      return true;
    };
    if constexpr (std::is_same_v<decltype(c), const Coord &>) {
      return Accepted(m[Coord2Idx(c)]);
    } else {
      return Accepted(m[c]);
    }
  };

  auto IsSpecial = [&Coord2Idx](const Maze &m, const auto &c) -> bool {
    auto Accepted = [](char v) {
      if ('.' == v)
        return false;
      if ('#' == v)
        return false;
      return true;
    };
    if constexpr (std::is_same_v<decltype(c), const Coord &>) {
      return Accepted(m[Coord2Idx(c)]);
    } else {
      return Accepted(m[c]);
    }
  };

  auto NeighbourCoord =
      [&Coord2Idx, &Idx2Coord](const auto &c, int dir) -> auto {
        Coord c_;
        if constexpr (std::is_same_v<decltype(c), const Coord &>) {
          c_ = c;
        } else {
          c_ = Idx2Coord(c);
        }
        switch (dir) {
          default:
          case 0:
            c_ = {c_.first - 1, c_.second};
            break;
          case 1:
            c_ = {c_.first, c_.second + 1};
            break;
          case 2:
            c_ = {c_.first + 1, c_.second};
            break;
          case 3:
            c_ = {c_.first, c_.second - 1};
            break;
        }
        if constexpr (std::is_same_v<decltype(c), const Coord &>) {
          return c_;
        } else {
          return Coord2Idx(c_);
        }
      };

  auto PrintMaze = [&lines, &linelen, &Coord2Idx](const Maze &m) -> void {
    for (int iy{0}; iy < lines; ++iy) {
      for (int ix{0}; ix < linelen; ++ix) {
        std::cout << m[Coord2Idx({ix, iy})];
      }
      std::cout << "\n";
    }
    std::cout << "\n";
  };

  auto EliminateDeadEnds =
      [&Coord2Idx, &Idx2Coord, &IsWall, &IsFreeSpace, &IsDoor, &NeighbourCoord](
          Maze &m) -> void {
        std::vector<bool> visited(m.size(), false);
        std::deque<int> queue;
        auto CheckDead =
            [&IsFreeSpace, &IsDoor, &IsWall, &NeighbourCoord](const Maze &m,
                int c) {
              if (IsFreeSpace(m, c) || IsDoor(m, c)) {
                int wallcount{0};
                for (int dir{0}; dir <= 3; ++dir) {
                  if (IsWall(m, NeighbourCoord(c, dir))) {
                    wallcount += 1;
                  }
                }
                if (wallcount >= 3) {
                  return true;
                }
              }
              return false;
            };
        for (int i = 0; i < m.size(); ++i) {
          if (CheckDead(m, i)) {
            visited[i] = true;
            queue.push_back(i);
          }
        }
        while (!queue.empty()) {
          int c = queue.front();
          queue.pop_front();
          m[c] = '#';
          for (int dir{0}; dir <= 3; ++dir) {
            int c_ = NeighbourCoord(c, dir);
            if (!visited[c_] && CheckDead(m, c_)) {
              visited[c_] = true;
              queue.push_back(c_);
            }
          }
        }
      };

  auto ScanCoords = [&Idx2Coord, &IsSpecial](const Maze &m) -> Coords {
    Coords result{};
    std::fill(result.begin(), result.end(), -1);
    for (size_t i{0}; i < m.size(); ++i) {
      if (IsSpecial(m, i)) {
        char c = m[i];
        if ('@' == c) {
          result[0] = i;
        } else if (islower(c)) {
          result[1 + (c - 'a')] = i;
        } else if (isupper(c)) {
          result[27 + (c - 'A')] = i;
        }
      }
    }
    return result;
  };

  auto ScanDists =
      [&Coord2Idx, &NeighbourCoord, &IsWalkable](const Maze &m, int c) {
        std::vector<int> result(m.size(), -1);
        std::vector<bool> visited(m.size(), false);
        visited[c] = true;
        std::deque<int> queue{c};
        result[c] = 0;
        while (!queue.empty()) {
          int cur = queue.front();
          queue.pop_front();
          int dist = result[cur];
          for (int dir{0}; dir <= 3; ++dir) {
            int next = NeighbourCoord(cur, dir);
            if (visited[next])
              continue;
            if (!IsWalkable(m, next))
              continue;
            queue.push_back(next);
            visited[next] = true;
            result[next] = dist + 1;
          }
        }
        return result;
      };

  auto AllKeys = [](const Coords &coords) -> KeySet {
    KeySet result{};
    for (int i{0}; i < 26; ++i) {
      result[i] = coords[i + 1] != -1;
    }
    return result;
  };

  Coords coords = ScanCoords(maze);
  KeySet allkeys = AllKeys(coords);

  auto Locks =
      [&ScanDists, &Idx2Coord, &NeighbourCoord, &IsWalkable](const Maze &m,
          const Coords &coords) -> LockedBy {
        LockedBy result{};
        int c = coords[0];
        std::vector<bool> visited(m.size(), false);
        std::vector<KeySet> locks_needed(m.size());
        visited[c] = true;
        std::deque<int> queue{c};
        while (!queue.empty()) {
          int cur = queue.front();
          queue.pop_front();
          char v = m[cur];
          if (isupper(v)) {
            locks_needed[cur][v - 'A'] = true;
          } else if (islower(v)) {
            result[v - 'a'] = locks_needed[cur];
          }

          for (int dir{0}; dir <= 3; ++dir) {
            int next = NeighbourCoord(cur, dir);
            if (visited[next])
              continue;
            if (!IsWalkable(m, next))
              continue;
            queue.push_back(next);
            visited[next] = true;
            locks_needed[next] = locks_needed[cur];
          }
        }
        return result;
      };

  auto Dists = [&ScanDists](const Maze &m, const Coords &coords) -> DistMatrix {
    DistMatrix result{};
    for (int src_{0}; src_ < 27; ++src_) {
      int src = coords[src_];
      if (-1 == src)
        continue;
      auto dists = ScanDists(m, src);
      for (int dst_{0}; dst_ < 27; ++dst_) {
        int dst = coords[dst_];
        if (-1 == dst)
          continue;
        result[src_][dst_] = dists[dst];
      }
    }
    return result;
  };

  LockedBy locked_by = Locks(maze, coords);

  DistMatrix dists = Dists(maze, coords);

  auto Obtainable =
      [&allkeys](const LockedBy &l, const KeySet &obtained) -> KeySet {
        KeySet result{};
        for (int i{0}; i < 26; ++i) {
          if (obtained[i] | !allkeys[i])
            continue;
          const auto &lb = l[i];
          result[i] = ((~lb | obtained).all());
        }
        return result;
      };

  auto BacktrackKeys = [&Obtainable](const Maze &m,
      const DistMatrix &d,
      const LockedBy &l) -> int {

    std::map<std::pair<unsigned long,int>, int> cache{};

    auto BackTrackKeys_ = [&Obtainable,&cache](const Maze &m,
        const DistMatrix &d,
        const LockedBy &l,
        KeySet obtained,
        int lastloc,
        auto &rec) -> int {
      int result{std::numeric_limits<int>::max()};
      auto obtainable = Obtainable(l, obtained);
      if (obtainable.none()) {
        return 0;
      } else {
        auto it = cache.find({obtained.to_ulong(), lastloc});
        if (it != cache.end()) {
          result = it->second;
        } else {
          std::vector<int> obtorder(obtainable.count(), 0);
          int idx{0};
          for (int i{0}; i < 26; ++i) {
            if (obtainable[i])
              obtorder[idx++] = i;
          }
          std::sort(obtorder.begin(),
              obtorder.end(),
              [&d, &lastloc](int a, int b) -> bool {
                int dista = d[lastloc][a + 1];
                int distb = d[lastloc][b + 1];
                return dista < distb;
              });
          for (int i : obtorder) {
            int dist = d[lastloc][i + 1];
            auto tmp = obtained;
            tmp[i] = true;
            int tmpres = rec(m, d, l, tmp, i + 1, rec) + dist;
            if (tmpres < result)
              result = tmpres;
          }
        }
        cache.emplace(std::pair{obtained.to_ulong(), lastloc}, result);
      }
      return result;
    };

    return BackTrackKeys_(m, d, l, {}, 0, BackTrackKeys_);
  };

  int mindist = BacktrackKeys(maze, dists, locked_by);

  std::cout << mindist << "\n";
}