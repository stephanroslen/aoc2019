#include <algorithm>
#include <array>
#include <iostream>
#include <numeric>

int main(int, const char *[]) {
  unsigned int cnt0{0};
  unsigned int cnt1{0};
  for (unsigned int i{193651}; i <= 649729; ++i) {
    std::array<int8_t, 6> val = {
        static_cast<int8_t>(i / 100000 % 10),
        static_cast<int8_t>(i / 10000 % 10),
        static_cast<int8_t>(i / 1000 % 10),
        static_cast<int8_t>(i / 100 % 10),
        static_cast<int8_t>(i / 10 % 10),
        static_cast<int8_t>(i / 1 % 10)};
    std::adjacent_difference(val.begin(), val.end(), val.begin());
    if (std::any_of(std::next(val.begin()),
        val.end(),
        [](int8_t v) -> bool { return v < 0; }))
      continue;
    if (0 == std::count(std::next(val.begin()), val.end(), 0))
      continue;
    cnt0 += 1;
    bool found{false};
    for (int j{1}; j < 6; ++j) {
      if ((0 == val[j]) && (0 != val[j-1]) && ((j == 5) || (val[j+1] != 0))) {
        found = true;
        break;
      }
    }
    if (!found)
      continue;
    cnt1 += 1;
  }
  std::cout << cnt0 << " " << cnt1 << "\n";
}