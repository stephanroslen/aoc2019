#include <algorithm>
#include <deque>
#include <iostream>
#include <iterator>
#include <locale>
#include <vector>

struct intcode : std::ctype<char> {
  intcode() : std::ctype<char>(get_table()) {}
  static std::ctype_base::mask const *get_table() {
    static std::vector<std::ctype_base::mask> rc(table_size,
        std::ctype_base::mask());

    rc[','] = std::ctype_base::space;
    rc['\n'] = std::ctype_base::space;
    rc[' '] = std::ctype_base::space;
    return rc.data();
  }
};

int main(int, const char *[]) {
  intcode loc{};
  std::cin.imbue(std::locale(std::locale(), &loc));
  std::deque<int> code{};
  std::copy(std::istream_iterator<int>(std::cin),
      std::istream_iterator<int>(),
      std::back_inserter(code));

  auto run = [](std::deque<int> code, int noun, int verb) -> int {
    constexpr int kInstAdd{1};
    constexpr int kInstMul{2};
    constexpr int kInstExit{99};

    code[1] = noun;
    code[2] = verb;
    unsigned int ip{0};
    while (code[ip] != kInstExit) {
      int inst = code[ip];
      unsigned int src0 = code[ip + 1];
      unsigned int src1 = code[ip + 2];
      unsigned int dst = code[ip + 3];
      switch (inst) {
        case kInstAdd:
          code[dst] = code[src0] + code[src1];
          break;
        case kInstMul:
          code[dst] = code[src0] * code[src1];
          break;
        default:
          std::cerr << "unknown: " << inst << "\n";
          break;
      }
      ip += 4;
    }
    return code[0];
  };

  std::cout << run(code, 12, 2) << "\n";

  for (int inoun{0}; inoun <= 99; ++inoun) {
    for (int iverb{0}; iverb <= 99; ++iverb) {
      if (19690720 == run(code, inoun, iverb)) {
        std::cout << 100 * inoun + iverb << "\n";
        return 0;
      }
    }
  }

  return 0;
}