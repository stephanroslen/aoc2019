#include <deque>
#include <fstream>
#include <iostream>
#include <optional>
#include <regex>
#include <streambuf>
#include <string>
#include <unordered_map>

struct Object {
  std::string name{};
  std::optional<std::string> parent{};
};

int main(int argc, const char *argv[]) {
  std::unordered_map<std::string, Object> objects{};
  assert(argc == 2);
  std::string data{};
  {
    std::ifstream datafile(argv[1]);
    if (!datafile.is_open()) {
      throw std::runtime_error("File not opened");
    }
    data.assign((std::istreambuf_iterator<char>(datafile)),
        std::istreambuf_iterator<char>());
  }

  const static std::regex re_orbit("([a-zA-Z0-9]+)\\)([a-zA-Z0-9]+)");
  std::for_each(std::sregex_iterator(data.begin(), data.end(), re_orbit),
      std::sregex_iterator(),
      [&](const auto &m) {
        std::string namel = m[1].str();
        std::string namer = m[2].str();
        auto &objl = objects[namel];
        auto &objr = objects[namer];
        objl.name = namel;
        objr.name = namer;
        objr.parent = namel;
      });

  size_t orbits{0};

  for (const auto &[name, object] : objects) {
    auto parent = object.parent;
    while (parent) {
      orbits += 1;
      parent = objects.at(*parent).parent;
    }
  }

  auto &you = objects.at("YOU");
  auto &santa = objects.at("SAN");

  auto collect_ancestors = [&](auto &obj) {
    std::deque<std::string> result{};
    auto parent = obj.parent;
    while (parent) {
      result.push_front(*parent);
      parent = objects.at(*parent).parent;
    }
    return result;
  };

  std::deque<std::string> ancyou = collect_ancestors(you);
  std::deque<std::string> ancsan = collect_ancestors(santa);

  while (ancyou.size() > 0 && ancsan.size() > 0
      && ancyou.front() == ancsan.front()) {
    ancyou.pop_front();
    ancsan.pop_front();
  }

  size_t transfers = ancyou.size() + ancsan.size();

  std::cout << orbits << " " << transfers << "\n";

  return 0;
}